A.Steps to run the project

 - Clone code from the following repo
 
 - git clone https://bitbucket.org/honguyenwizetest/wizeline-qa-onsite.git
 
 - Open the project
 
 - Resolve dependencies from POM file
 
 - Run StepDefs
 
B.Assignment

1. Correct the failed case removeTheLastAPIKey

2. Create test automation for editing the last api key

3. After executing auto test cases, there are many openning browsers. How to improve it?

4. Support the configuration for hard code information such as URL, username, password

5. Organize the project regarding page object model

